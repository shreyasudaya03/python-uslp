# Project XXX

How to use this template:

- Clone or download it. If you cloned it, then wipe out the git history with `rm -rf .git`.
- Do `git init`.
- Modify all files that contain xxx with your project details.
- Do `git add .` and `git commit -m "Initial commit"`.
- Create on gitlab your new prototype project.
- Add the remote repo to your local repo: `git remote add origin https://...`.
- Do `git push`.

## Installation

Clone the repository and then install via pip:

```
$ git clone https://gitlab.com/librecube/prototypes/xxx
$ cd xxx
$ python -m venv venv
$ . venv/bin/activate
$ pip install -e .
```

## Example

To be written...

## Contribute

- Issue Tracker: https://gitlab.com/librecube/prototypes/xxx/-/issues
- Source Code: https://gitlab.com/librecube/prototypes/xxx

To learn more on how to successfully contribute please read the contributing
information in the [LibreCube documentation](https://librecube.gitlab.io/).

## Support

If you are having issues, please let us know. Reach us at
[Matrix](https://app.element.io/#/room/#librecube.org:matrix.org)
or via [Email](mailto:info@librecube.org).

## License

The project is licensed under the MIT license. See the [LICENSE](./LICENSE.txt) file for details.
